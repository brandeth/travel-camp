import type { Metadata } from "next";
import "./globals.css";
import Navbar from "@/components/Navbar";
import Footer from "@/components/Footer";

export const metadata: Metadata = {
  title: "Travel Camp",
  description: "UI/UX Design for Travel Camp from JavaScript Mastery YouTube",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <Navbar />
        <main className="overflow-hidden relative">{children}</main>
        <Footer />
      </body>
    </html>
  );
}
